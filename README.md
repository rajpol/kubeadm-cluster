# Kubeadm cluster

This playbook repo sets up a single control-plane kubernetes cluster on CentOS 7, according to the official install docs. This is not an alternative to kubespray, I'm only supporting a specific set of technologies.

The goal is to have a complete devops environment where developers can;

1. Run CI/CD pipelines from Gitlab.
2. Package Helm charts for deployment.
3. Deploy services using those charts in the k8s cluster.

## Features deployed by these playbooks so far

- [x] Etcd "in-band" bundled with kubeadm.
- [x] Kubeadm/kubelet/kube-proxy and other standard services.
- [x] Calico pod networking with iptables.
- [x] Helm and helm repos.
- [x] Ingress-nginx as an optional feature installed as helm chart.
- [x] Service accounts to be used for CI/CD deploys for example.
- [x] Generating kubeconfig files for service accounts.
- [x] Cert-manager and self-signed Issuer.
- [x] MetalLB setup using IPs defined by ``metallb_addresses`` in ``inventory/default/group_vars/k8s_cluster.yml``.
- [ ] HA control plane.
- [ ] NetworkPolicy for isolated namespaces.
- [ ] Prometheus operator+grafana.
- [ ] Automate [kubeadm join on new nodes](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/#automating-kubeadm)

## Quickstart

1. Look at ``inventory/default/group_vars/k8s_cluster.yml``.
2. [Install Ansible dependencies](#install-ansible-dependencies).
3. On a host with libvirt run ``vagrant up``.
    * This vagrant setup was tested on Fedora with libvirt.
    * Your libvirt has a network named ``default`` with the range ``192.168.122.0/24``.
    * If you use a different subnet, edit ``metallb_addresses`` in inventory to set a different VIP for MetalLB.
    * Unless you specify ``--no-provision`` it will run bootstrap.yml, which you can also run on other inventory environments.
4. Run ``vagrant ssh-config`` to find your master IP and store your ssh config for future Ansible runs.
5. Run ``control-plane.yml`` to setup master nodes, at this time no HA support yet but it will attempt to install each master node you specify in inventory.
    * Take note of token and cert hash output by ``control-plane.yml``.
6. Run ``join-cluster.yml`` [with values](#join-workers) from step 4 to join workers to cluster.
7. Wait for kubectl get nodes to be Ready.
8. Run more playbooks to setup Helm charts, manifests, kustomizations, see more below.

## Detailed vagrant setup

[![Animation of me following the quickstart steps](https://asciinema.org/a/IlkJD98cz6hAQrUjDSUcc3aBK.svg)](https://asciinema.org/a/IlkJD98cz6hAQrUjDSUcc3aBK)

### All settings are in inventory group vars

* Recommended to read through the comments in ``inventory/default/group_vars/k8s_cluster.yml``
* Optionally copy default to your own environment name, and set ``site_environment`` in hosts file to your new environment name.
* Optionally look at ``vars/default.yml`` for some settings only used by bootstrap.yml and specific to vagrant.

### Install Ansible dependencies

This installs my own roles hosted [on Gitlab](https://gitlab.com/stemid-ansible/roles).

    $ ansible-galaxy install -r requirements.yml
    $ pip install --user jmespath

``python3-jmespath`` is used by the ``json_query`` jinja filter to extract data from JSON.

### This will run bootstrap.yml and install necessary container services

    $ vagrant up

#### If you're not running vagrant, run bootstrap.yml manually

    $ ansible-playbook -i inventory/default/hosts bootstrap.yml

### Configure your SSH access to the nodes.

Add vagrant.conf to your ``~/.ssh/config`` as an Include.

    $ vagrant ssh-config > ~/.ssh/vagrant.conf

### Init master node and prepare workers

    $ ansible-playbook -i inventory/default/hosts control-plane.yml

* It should print a token and cert hash value at the end.
* Write these down to easily join new workers in the future.
* It should be safe to re-run control-plane.yml and get the values again.
* If kubeadm init fails try running ``kubeadm reset`` manually, check your config, logs and try again.

### Join workers

    $ ansible-playbook -i inventory/default/hosts join-cluster.yml -e 'token=abc.123' \
      -e 'cert_hash=sha256:xxx' \
      -e 'k8s_apiserver_advertise_address=192.168.122.xx'

* *Important* that you set ``k8s_apiserver_advertise_address`` to the IP of your master node.
* This is a manual standalone step so that you can write down your token and cert hash safely and join new nodes when you want to scale up. Just add them to your hosts inventory and run this again.

### Deploy administrator account

    $ ansible-playbook -i inventory/default/hosts system-services.yml -t manifests

This sets up the default administrator account defined in the ``k8s_service_accounts`` list of ``inventory/default/groups_vars/k8s_cluster.yml``.

>system-services.yml is a playbook that installs a lot of different things, [read more about it here](#deploy-some-essential-system-services-in-the-cluster).

### Get kubeconfig for administrator account

    $ ansible-playbook -i inventory/default/hosts kubeconfigs.yml

This downloads a kubeconfig into ``files/default/``

### Test cluster with kubectl

    $ kubectl --kubeconfig files/default/kubeconfig.yaml get pods -A

### Configure kubectl

This should be saved in your shell profile but here's a quick way to get access to the cluster from your vagrant host. You must have [kubectl installed obviously](https://kubernetes.io/docs/tasks/tools/install-kubectl/).

    $ mkdir $HOME/.kube
    $ cp files/default/kubeconfig.yaml $HOME/.kube/vagrant-kubeconfig.yaml
    $ export KUBECONFIG=$KUBECONFIG:$HOME/.kube/vagrant-kubeconfig.yaml
    $ kubectl config use-context kubernetes-admin@kubernetes
    $ kubectl get nodes
    NAME       STATUS   ROLES    AGE     VERSION
    master     Ready    master   5d10h   v1.18.2
    worker01   Ready    <none>   5d10h   v1.18.2
    worker02   Ready    <none>   5d10h   v1.18.2
    $ kubectl get pods -A
    ...

>It might take several minutes before kubelet logs stop reporting networking errors, all calico pods are deployed and all nodes are Ready.

## Optional

### Further configuration of your environment

* ``cp -r inventory/default inventory/myenv``
* Open ``inventory/myenv/group_vars/k8s_cluster.yml`` read and edit it to fit your environment and wishes. It's well commented.
* Edit ``inventory/myenv/hosts`` to match your own hosts.

### Deploy some essential system services in the cluster

* Things like nginx-ingress controller and MetalLB are enabled by default.
* Check that ``metallb_addresses`` does not clash with any existing IPs.
* Wait between deploying tag helm and tag manifests because Helm charts include cert-manager and manifests include clusterissuer.yaml which depends on cert-manager being ready.
* Enable helm charts like kubernetes-dashboard and cert-manager by changing the state from skip to present.
* Enable manifests like the default clusterissuer.yaml and metrics-server.yaml for use with cert-manager and dashboard.
* Run with --skip-tags manifests first because clusterissuer.yaml requires cert-manager helm chart to be installed.

```
$ ansible-playbook -i inventory/default/hosts system-services.yml --skip-tags manifests
$ ansible-playbook -i inventory/default/hosts system-services.yml -t manifests
```

* Now you should be able to visit the dashboard, its hostname is by default [dashboard.cluster.local](https://dashboard.cluster.local).
* Use the token in ``files/default/administrator-kubeconfig.yaml`` to login.
* Read the user defined service instructions about hostname because the same applies to ``dashboard.cluster.local``.

#### Explanation of system-services.yml

Deploys many different things by using ansible tags, here is a list;

* helm charts (helm, charts)
* manifests (manifests)
* kubectl krew (kubectl, krew)
* kubectl krew plugins (krew)
* kubernetes operators (operators)

##### Tips

* Read the comments in your ``group_vars/k8s_cluster.yml`` file first of all.
* One example manifest deploys service accounts: ``templates/manifests/service-accounts.yaml``.
* Running kubeconfigs.yml you can create kubeconfig files for these service accounts and download them to ``files/``.
* Define your service accounts using ``k8s_service_accounts`` in your ``inventory/default/group_vars/k8s_cluster.yml`` file.

```
$ ansible-playbook -i inventory/default/hosts kubeconfigs.yml
$ cp files/default/myaccount-kubeconfig.yaml ~/.kube/
$ export KUBECONFIG=$KUBECONFIG:$HOME/.kube/myaccount-kubeconfig.yaml
```

### Deploy an example user defined service based on Python and Flask in k8s

    $ ansible-playbook -i inventory/default/hosts user-services.yml
    $ kubectl get svc nginx-ingress-controller
    NAME                       TYPE           CLUSTER-IP       EXTERNAL-IP     PORT(S)                      AGE
    nginx-ingress-controller   LoadBalancer   10.107.232.195   192.168.122.2   80:31846/TCP,443:31876/TCP   45s
    $ curl -sLD - -H 'Host: flask-boilerplate.cluster.local' 'http://192.168.122.2/api/v1/client/ip'
    HTTP/1.1 200 OK
    Server: nginx/1.17.10
    Date: Wed, 08 Jul 2020 19:33:13 GMT
    Content-Type: application/json
    Content-Length: 68
    Connection: keep-alive

    {"ip":"10.0.12.65","nodename":"flask-boilerplate-8548b6cdd5-mvtvp"}

* Note that the EXTERNAL-IP might differ for you.
* The above is just an example, with cert-manager you'll get a 308 redirect to https which would require the -k option if you're using the default selfsigned-issuer.
* If you set it up in your hosts file you can request ``http://flask-boilerplate.cluster.local`` with curl directly.
* Run the curl request multiple times to see how the service load balances traffic to more than one pod (nodename).

### Exposing kube API

In short;

* Install Traefik
* Create a Service for kube-apiserver
* Hook a Traefik TCP Ingress route to the kube-apiserver Service
* Reconfigure CertSANs in master nodes

#### Reconfigure certSANs

Short summary of [this blog post](https://blog.scottlowe.org/2019/07/30/adding-a-name-to-kubernetes-api-server-certificate/), do these steps on one of the masters;

* Create a kubeadm.yaml configuration file of the current running config: ``kubectl -n kube-system get configmap kubeadm-config -o jsonpath='{.data.ClusterConfiguration}' > kubeadm.yaml``
* Add a ``apiServer.certSANs`` list of all the additional TLS names you want the kube apiserver to handle, this is where you put your external domain or IP that is routed by Traefik to your kube-apiserver Service.
* Move the old cert files which will trigger kubeadm to generate new certs: ``mv /etc/kubernetes/pki/apiserver.{crt,key} ~/Backups/``
* Re-run the certs phase of kubeadm init to re-generate certs: ``kubeadm init phase certs apiserver --config kubeadm.yaml``
* Force re-start of apiserver container to make it load new certs: ``docker kill $(docker ps | grep kube-apiserver | grep -v pause | awk -F '{print $1}')``

## Upgrading

* **WARNING** My opinion is that it's not safe to automate the upgrade process with Ansible yet.
* Follow the [official upgrade docs](https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/).

Certain steps can be automated like for example;

    $ ansible -i inventory/myenv/hosts -m yum -a 'name=kubeadm-1.18.2-0 state=present' workers -b

And after running some manual steps from the docs;

    $ ansible -i inventory/myenv/hosts -m yum -a 'name=kubelet-1.18.2-0 state=present' workers -b
    $ ansible -i inventory/myenv/hosts -m yum -a 'name=kubectl-1.18.2-0 state=present' workers -b

## Kubernetes Tutorials

Once you've deployed a cluster to vagrant you can start with [this Kubernetes tutorial](https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/) and follow the steps in your own cluster.

Note that sometimes the interactive tutorial will tell you to call a pod service through kube-proxy using a command like this.

    $ curl "http://localhost:8001/api/v1/namespace/default/pod/$POD_NAME/proxy/"

And if that doesn't work because your pod is using port 8080 then you can simply add the port like this.

    $ curl "http://localhost:8001/api/v1/namespace/default/pod/$POD_NAME:8080/proxy/"

